package com.waracle.androidtest.ui;
/**
 * Created by arkangel on 13/05/15.
 */

import android.annotation.TargetApi;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.waracle.androidtest.R;
import com.waracle.androidtest.adapters.StickyListAdapter;
import com.waracle.androidtest.utils.CroutonUtils;
import com.waracle.backend.builders.CakeDataGetRequestBuilder;
import com.waracle.backend.model.ApiResponseModel_Cake;
import com.waracle.backend.utils.DeviceUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class StickyCakeHeaderList extends Fragment implements AbsListView.OnScrollListener,
        StickyListHeadersListView.OnStickyHeaderOffsetChangedListener,
        StickyListHeadersListView.OnStickyHeaderChangedListener {

    private static final String USERNAME = "hart88";
    private static final String HASH_ONE = "198f29ec5114a3ec3460";
    private static final String HASH_TWO = "8dd19a88f9b8d24c23d9960f3300d0c917a4f07c";
    private static final int ANIMATION_DURATION = 300;
    @InjectView(R.id.cake_list_view) StickyListHeadersListView matchListView;
    @InjectView(android.R.id.empty) View emptyStateView;
    private int bigHeight;
    private int smallHeight;
    private boolean fadeHeader = true;

    private String[] mGroups;

    private boolean footerVisible;

    private View view;
    private StickyListAdapter adapter;

    private View currentlyStickyHeader;
    private int currentlyStickyHeaderPosition;

    public static StickyCakeHeaderList getInstance() {
        StickyCakeHeaderList stickyHeaderMatchList = new StickyCakeHeaderList();
        return stickyHeaderMatchList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.screen_cake_list, container, false);
        ButterKnife.inject(this, view);
        adapter = new StickyListAdapter(getActivity());
        matchListView.setEmptyView(emptyStateView);
        matchListView.setOnStickyHeaderChangedListener(this);
        matchListView.setOnStickyHeaderOffsetChangedListener(this);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (DeviceUtils.isConnected(getActivity())) {
            CakeDataGetRequestBuilder.newBuilder(USERNAME, HASH_ONE, true, HASH_TWO).setResponseListener(new Response.Listener<ApiResponseModel_Cake[]>() {
                @Override
                public void onResponse(ApiResponseModel_Cake[] apiResponseModelCakes) {
                    List<ApiResponseModel_Cake> apiResponseModel_cakeArrayList = new ArrayList<ApiResponseModel_Cake>(apiResponseModelCakes.length);
                    Collections.addAll(apiResponseModel_cakeArrayList, apiResponseModelCakes);
                    adapter.addAll((ArrayList<ApiResponseModel_Cake>) apiResponseModel_cakeArrayList);
                    matchListView.setAdapter(adapter);
                    Log.i(StickyCakeHeaderList.class.getSimpleName(), apiResponseModelCakes.toString());
                }
            }, ApiResponseModel_Cake[].class).setErrorListener(new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    Log.i(StickyCakeHeaderList.class.getSimpleName(), volleyError.getMessage());
                }
            }).allowCache(true).
                    setTag(StickyCakeHeaderList.class.getSimpleName().toString()).build().execute();
        } else {
            CroutonUtils.error(getActivity(), "No internet available");
            ((TextView) view.findViewById(R.id.no_data_textView)).setText("No Internet");
            ((ProgressBar) view.findViewById(R.id.progressbar)).setVisibility(View.GONE);
            matchListView.setEmptyView(emptyStateView);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onStickyHeaderOffsetChanged(StickyListHeadersListView l, View header, int offset) {
        if (fadeHeader && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            header.setAlpha(1 - (offset / (float) header.getMeasuredHeight()));
        }
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onStickyHeaderChanged(StickyListHeadersListView l, View header, int itemPosition, long headerId) {
        header.setAlpha(1);
        currentlyStickyHeader = header;
        currentlyStickyHeaderPosition = l.getFirstVisiblePosition();
        setStickyHeaderHeight(bigHeight);
    }

    public void setStickyHeaderHeight(int stickyHeaderHeight) {
        ViewGroup.LayoutParams lp = currentlyStickyHeader.getLayoutParams();
        lp.height = stickyHeaderHeight;
        currentlyStickyHeader.setLayoutParams(lp);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // don't care
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (currentlyStickyHeader == null || currentlyStickyHeaderPosition != firstVisibleItem) {
            return;
        }
        int height = bigHeight + view.getChildAt(0).getTop();
        if (height < smallHeight) {
            height = smallHeight;
        }
        setStickyHeaderHeight(height);
    }
}

