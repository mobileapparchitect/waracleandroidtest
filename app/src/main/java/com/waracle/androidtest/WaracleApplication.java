package com.waracle.androidtest;

import com.waracle.backend.WaracleMobileApplication;

public class WaracleApplication extends WaracleMobileApplication {

    private static WaracleApplication singleton;

    public static WaracleApplication getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
    }
}
