package com.waracle.androidtest.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.waracle.androidtest.R;
import com.waracle.backend.model.ApiResponseModel_Cake;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

public class StickyListAdapter extends BaseAdapter implements View.OnClickListener, StickyListHeadersAdapter, SectionIndexer {

    private Context mContext;

    private LayoutInflater mInflater;

    private Character[] rows;
    private int[] mSectionIndices;
    private ArrayList<Long> listOfheaders;
    private ArrayList<ApiResponseModel_Cake> listOfCakes;

    public StickyListAdapter(Context context) {
        this.mContext = context;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return listOfCakes.size();
    }

    @Override
    public Object getItem(int position) {
        return listOfCakes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void allTitleToUppercase() {
        for (ApiResponseModel_Cake cake : listOfCakes) {
            cake.setTitle(cake.getTitle().toUpperCase());
        }
    }

    public void addAll(ArrayList<ApiResponseModel_Cake> items) {
        listOfCakes = items;
        allTitleToUppercase();
        mSectionIndices = getSectionIndices();
        rows = getSectionLetters();
        listOfheaders = new ArrayList<>();
        setUpHeaders(listOfCakes);
        notifyDataSetChanged();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    public void setUpHeaders(ArrayList<ApiResponseModel_Cake> parseObjectArrayList) {
        for (int count = 0; count < parseObjectArrayList.size(); count++) {
            ApiResponseModel_Cake apiResponseModel_cake = (ApiResponseModel_Cake) parseObjectArrayList.get(count);
            long hashCode = (long) apiResponseModel_cake.getTitle().hashCode();
            listOfheaders.add(hashCode);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.view_cake_row, null);
            holder = new ViewHolder();
            holder.description = (TextView) convertView.findViewById(R.id.description);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.imageView = (ImageView) convertView.findViewById(R.id.image_view);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ApiResponseModel_Cake apiResponseModel_cake = (ApiResponseModel_Cake) getItem(position);
        holder.description.setText(apiResponseModel_cake.getDesc());
        holder.title.setText(apiResponseModel_cake.getTitle());
        if (!TextUtils.isEmpty(((ApiResponseModel_Cake) getItem(position)).getImage())) {
            Glide.with(mContext)
                    .load(apiResponseModel_cake.getImage())
                    .into(holder.imageView);
        }
        return convertView;
    }

    private int[] getSectionIndices() {
        ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
        char lastFirstChar = ((ApiResponseModel_Cake) getItem(0)).getTitle().charAt(0);
        sectionIndices.add(0);
        for (int i = 1; i < listOfCakes.size(); i++) {
            if (listOfCakes.get(i).getTitle().charAt(0) != lastFirstChar) {
                lastFirstChar = listOfCakes.get(i).getTitle().charAt(0);
                sectionIndices.add(i);
            }
        }
        int[] sections = new int[sectionIndices.size()];
        for (int i = 0; i < sectionIndices.size(); i++) {
            sections[i] = sectionIndices.get(i);
        }
        return sections;
    }

    @Override
    public View getHeaderView(int i, View convertView, ViewGroup parent) {
        HeaderViewHolder headerViewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.header_layout, parent, false);
            headerViewHolder = new HeaderViewHolder();
            headerViewHolder.headerView = (TextView) convertView.findViewById(R.id.header);
            headerViewHolder.headerView.setClickable(false);
            convertView.setTag(headerViewHolder);
        } else {
            headerViewHolder = (HeaderViewHolder) convertView.getTag();
        }
        CharSequence headerChar = listOfCakes.get(i).getTitle().subSequence(0, 1);
        headerViewHolder.headerView.setText(headerChar);
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return listOfheaders.get(position);
    }

    @Override
    public Object[] getSections() {
        return rows;
    }

    @Override
    public int getPositionForSection(int position) {
        for (int i = 0; i < mSectionIndices.length; i++) {
            if (position < mSectionIndices[i]) {
                return i - 1;
            }
        }
        return mSectionIndices.length - 1;
    }

    private Character[] getSectionLetters() {
        Character[] letters = new Character[mSectionIndices.length];
        for (int i = 0; i < mSectionIndices.length; i++) {
            letters[i] = listOfCakes.get(i).getTitle().charAt(0);
        }
        return letters;
    }

    @Override
    public int getSectionForPosition(int section) {
        if (mSectionIndices.length == 0) {
            return 0;
        }
        if (section >= mSectionIndices.length) {
            section = mSectionIndices.length - 1;
        } else if (section < 0) {
            section = 0;
        }
        return mSectionIndices[section];
    }

    @Override
    public void onClick(View view) {
    }

    public class ViewHolder {

        TextView title;
        TextView description;
        ImageView imageView;
    }

    public class HeaderViewHolder {

        TextView headerView;
    }
}
