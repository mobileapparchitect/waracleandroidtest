package com.waracle.backend.builders;

public class CakeDataGetRequestBuilder extends GetRequestBuilder {


    public CakeDataGetRequestBuilder(String username, String hashOne, boolean raw, String hashTwo) {

        super();
        StringBuilder builder = new StringBuilder();

        builder.append("/");
        builder.append(username);
        builder.append("/");
        builder.append(hashOne); builder.append("/");
        if(raw){
            builder.append("raw");
            builder.append("/");
        }
        builder.append(hashTwo);


        url += builder.toString() + RequestMethods.METHOD_CAKE;
    }

    public static CakeDataGetRequestBuilder newBuilder(String username, String hashOne, boolean raw, String hashTwo) {
        return new CakeDataGetRequestBuilder(username, hashOne, raw,hashTwo);
    }
}
