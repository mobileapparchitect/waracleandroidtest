package com.waracle.backend.builders;


import com.android.volley.Request;
import com.waracle.backend.network.ServicesUtils;

public  class GetRequestBuilder extends BaseRequestBuilder {
        public GetRequestBuilder() {
            setMethod(Request.Method.GET);
        }

        @Override
        public WaracleApiRequest build() {
            encodeParams();
            checkPreconditions();
            return new WaracleApiRequest(url, method, listener, responseType, errorListener,  ServicesUtils.ListToHashTable(params), tag, allowFromCache);
        }
    }