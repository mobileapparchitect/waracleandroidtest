package com.waracle.backend.builders;



import com.android.volley.Request;
import com.android.volley.Response;
import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.waracle.backend.WaracleMobileApplication;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import java.util.LinkedList;
import java.util.List;



public abstract class BaseRequestBuilder {

    protected String url; //REQUIRED
    protected Response.Listener listener; //REQUIRED
    protected Class responseType; //REQUIRED
    protected Response.ErrorListener errorListener; //REQUIRED
    protected int method; //REQUIRED
    protected List<NameValuePair> params; //REQUIRED
    protected String tag; //REQUIRED
    protected boolean allowFromCache; // OPTIONAL
    private boolean checkParams;

    protected BaseRequestBuilder() {
        checkParams = false;
        url = WaracleMobileApplication.getInstance().getAppBaseUrl();
        method = -1;
        allowFromCache = false;
        params = new LinkedList<>();

    }

    public BaseRequestBuilder setResponseListener(Response.Listener listener, Class responseType) {
        this.listener = listener;
        this.responseType = responseType;
        return this;
    }

    public BaseRequestBuilder setErrorListener(Response.ErrorListener errorListener) {
        this.errorListener = errorListener;
        return this;
    }

    protected BaseRequestBuilder setMethod(int method) {
        this.method = method;
        return this;
    }

    public BaseRequestBuilder setTag(String tag) {
        this.tag = tag;
        return this;
    }

    public BaseRequestBuilder allowCache(boolean allowFromCache) {
        this.allowFromCache = allowFromCache;
        return this;
    }

    protected void encodeParams() {
        String paramString = URLEncodedUtils.format(params, Charsets.UTF_8.name());
        if (!params.isEmpty()) {
            url += "?" + paramString;
        }
    }

    protected void encodeBibleParams() {
    }

    public void setCheckParamsFlag(boolean checkParams) {
        this.checkParams = checkParams;
    }

    protected void checkPreconditions() {
        Preconditions.checkNotNull(listener, "listener not set");
        Preconditions.checkNotNull(responseType, "responseType not set");
        Preconditions.checkNotNull(errorListener, "errorListener not set");
        Preconditions.checkNotNull(tag, "tag not set");
        Preconditions.checkArgument(method != -1, "method not set");
        if (checkParams) {
            Preconditions.checkArgument(method == Request.Method.POST ? params.size() > 3 : params.size() > 1,
                    "no params have been set");
        }
    }

    public abstract WaracleApiRequest build();
}
