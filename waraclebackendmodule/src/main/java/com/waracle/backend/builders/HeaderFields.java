package com.waracle.backend.builders;


public interface HeaderFields {

    public static final String AUTHORIZATION = "Authorization";
    public static final String ENCODING_GZIP = "gzip";
    public static final String CONTENT_ENCODING = "Content-Encoding";

}
