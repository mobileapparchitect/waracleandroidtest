package com.waracle.backend.model;

import android.os.Parcelable;
import android.os.Parcel;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiResponseModel_Cake implements Parcelable{

    @JsonProperty("title")
    private static final String FIELD_TITLE = "title";
    @JsonProperty("desc")
    private static final String FIELD_DESC = "desc";
    @JsonProperty("image")
    private static final String FIELD_IMAGE = "image";


    private String mTitle;
    private String mDesc;
    private String mImage;


    public ApiResponseModel_Cake(){

    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setDesc(String desc) {
        mDesc = desc;
    }

    public String getDesc() {
        return mDesc;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getImage() {
        return mImage;
    }

    public ApiResponseModel_Cake(Parcel in) {
        mTitle = in.readString();
        mDesc = in.readString();
        mImage = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<ApiResponseModel_Cake> CREATOR = new Parcelable.Creator<ApiResponseModel_Cake>() {
        public ApiResponseModel_Cake createFromParcel(Parcel in) {
            return new ApiResponseModel_Cake(in);
        }

        public ApiResponseModel_Cake[] newArray(int size) {
        return new ApiResponseModel_Cake[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mDesc);
        dest.writeString(mImage);
    }


}