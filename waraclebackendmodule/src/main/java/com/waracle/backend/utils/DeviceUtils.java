package com.waracle.backend.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import com.waracle.backend.logs.Log;

public class DeviceUtils {

    private static final String LOG_TAG = DeviceUtils.class.getSimpleName();

    public static boolean isConnected(Context context) {
        if (context == null) {
            Log.e("Context is null; can't get connection status");
            return true;
        } else {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnected();
        }
    }

    public static boolean isConnectedToPhoneNetwork(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getNetworkType() != TelephonyManager.NETWORK_TYPE_UNKNOWN;
    }
}